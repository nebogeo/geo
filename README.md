About
=====

Geo is a simple, experimental game that uses a type of genetic
programming to grow enemies to shoot at (and that shoot at you).

Your job is to collect all the gravitational energy units to progress to
the next level. Your enemies (the geo entities) develop by passing the
code used to describe their form on to offspring, which are spawned once
they gain enough energy. This code is mutated slightly each time,
meaning that a kind of evolution can take place. The path of this
development is partially governed by your actions, but generally, geo
entities emerge that populate space dangerously quickly. You will need
to find ways to control their numbers for you to complete your mission.

Due to the nature of random selection at the start of the first level
(the initial enemy entities are entirely randomly created), games can be
quite variable in terms of difficulty.

Everything in this game is created procedurally, from the geo's forms to
the sound effects.

Instructions
============

There is no mouse control, use cursor keys to play the game, and to use
the menu system. More details can be found in the information section on
the game menu.

Controlling your ship:
w: Thrust forward
a/d: rotate ship
s: braking thruster
space: fire weapon
number keys: pick weapons, they will be enabled as you collect them.
f: fullscreen mode
g: windowed mode
p: pause/unpause
q: quit game

Hint: Some of the harmless geo entities are quite harmless and are
useful as an energy source if you land on them.

The green dots in the backgound comprise your scanner. Small dots are
geo entities, larger dots represent gravitational energy items that you
have to collect. Collect the star shaped pickups to increase your weapon
arsenal.

The energy items and weapon pickups will orbit chaotically around the
geo colonies. Some are faster moving than others, you will have to
develop your flying skills to collect them.

When you fly near enough to the energy items, they will switch to
following your ship. As well as allowing you to leave the level when you
have collected them all, the energy items have another use. If you fly
too close to the geo's defensive modules they will fire homing missiles
at your ship. The captured energy items in your possesion will shield
you from these attacks.

Todo list
=========

* HUD work
* Points scoring?
* More audio
* 4 more weapons needed (quarantine thing)
* windows fixes - static link
* finish info text + display graphics explanation
* Improve recharging (just on blue components?)
* splash screen while generating audio
