/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include <sys/time.h>
#include <list>
#include <set>
#include "geo.h"
#include "projectiles.h"
#include "particles.h"

#ifndef GEOGAME
#define GEOGAME

namespace geogame {

class geo_game
{
public:
	geo_game(int w, int h, char *n);
	virtual ~geo_game() {}

	void init();
	void handle(unsigned char key, int button, int state, int x, int y);
	void update();
	void reshape(int width, int height);
	void render();
	bool finished();
	void add_entity(entity *e) { m_world.push_back(e); }

private:
	void render_player();
	void renderHUD();
	void init_audio();
	void clear();
	void make_new_level();
	void display_text(string text, int frames);
	void render_text();

	int m_last_mouseX;
	int m_last_mouseY;
	float m_rotX,m_rotY,m_posX,m_posY,m_disY;
	bool m_display_genes;
	set<int> m_pressed_keys;

	list<entity*> m_world;
	vector<geo::genome> m_store;

	particles *m_thrust_particles;

	int m_width,m_height;
	float m_scale;

	int m_weapon_recharge;

	d_vector m_player_pos;
	d_vector m_player_dir;
	d_vector m_player_vec;

	bool m_first_time;
	float m_player_health;
	int m_level;
	int m_lives;
	bool m_finished;
	bool m_level_complete;
	int m_delay;
	bool m_life_lost;
	bool m_flash;
	int m_flash_counter;
	bool m_recharging;
	int m_weapon_level;
	int m_current_weapon;
	unsigned int m_frame;
	float m_weapon_energy[10];

	string m_display_text;
	int m_display_text_frames;
	bool m_pause_mode;

	int m_spawn_sound;
	int m_fire_sound;
	int m_geo_explosion_sound;
	int m_explosion_sound;
	int m_capture_sound;
	int m_mute_sound;
	int m_BGsound;

	int m_BGsoundID;
	timeval m_last_time;
	float m_deadline;
	double m_time;
	double m_delta;
};
}
#endif
