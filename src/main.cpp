/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "glinc.h"
#include <unistd.h>
#include "strings.h"
#include "dada.h"
#include "geogame.h"
#include "intro.h"

using namespace geogame;
using namespace std;

geo_game *my_geo_game;
intro *my_geo_intro;

unsigned int fps = (int)((1/60.0f)*1000.0f);
bool game_mode=false;

void timer_callback(int arg)
{
  if (game_mode) {
    my_geo_game->update();
    my_geo_game->render();
    if (my_geo_game->finished()) {
      my_geo_intro->init();
      game_mode=false;
    }
  } else {
    my_geo_intro->update();
    my_geo_intro->render();
    if (my_geo_intro->finished()) {
      my_geo_game->init();
      game_mode=true;
    }
  }
  
  glutSwapBuffers();
  glutTimerFunc(fps,timer_callback,0);
}

void display_callback()
{
}

void reshape_callback(int width, int height)
{
  my_geo_game->reshape(width,height);
  my_geo_intro->reshape(width,height);
}

void keyboard_callback(unsigned char key,int x, int y)
{
  if (game_mode) my_geo_game->handle(key, 0, 0, x, y);
  else my_geo_intro->handle(key, 0, 0, x, y);

  if (key=='f') {
    glutFullScreen();
  }

  if (key=='g') {
    glutReshapeWindow(640,480);
    glutPositionWindow(100,100);
  }
}

void keyboard_up_callback(unsigned char key,int x, int y)
{
  if (game_mode) my_geo_game->handle(key, 0, 1, x, y);
  else my_geo_intro->handle(key, 0, 1, x, y);
}

void special_keyboard_callback(int key,int x, int y)
{
  if (game_mode) my_geo_game->handle(key, 0, 0, x, y);
  else my_geo_intro->handle(key, 0, 0, x, y);
}

void special_keyboard_up_callback(int key,int x, int y)
{
  if (game_mode) my_geo_game->handle(key, 0, 1, x, y);
  else my_geo_intro->handle(key, 0, 1, x, y);
}

void mouse_callback(int button, int state, int x, int y)
{
  if (game_mode) my_geo_game->handle(0, button, state, x, y);
  else my_geo_intro->handle(0, button, state, x, y);
}

void motion_callback(int x, int y)
{
  if (game_mode) my_geo_game->handle(0, 0, 0, x, y);
  else my_geo_intro->handle(0, 0, 0, x, y);
}

void idle_callback()
{
  glutPostRedisplay();
}


int main(int argc, char *argv[])
{
  srand(time(NULL));
  init_dada();

  int width=160*6;
  int height=90*6;

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE);
  glutInitWindowSize(width, height);

  int win = glutCreateWindow("geo");
  my_geo_game = new geo_game(width,height,"geo");
  my_geo_intro = new intro(width,height,my_geo_game);

  my_geo_intro->build("todo",TODO_SCR);
  my_geo_intro->build("about",ABOUT_SCR);
  my_geo_intro->build("info1",INFO_SCR1);
  my_geo_intro->build("info2",INFO_SCR2);
  my_geo_intro->build("info3",INFO_SCR3);
  my_geo_intro->build("title",TITLE_SCR);

  glutDisplayFunc(display_callback);
  glutReshapeFunc(reshape_callback);
  glutKeyboardFunc(keyboard_callback);
  glutSpecialFunc(special_keyboard_callback);
  glutMouseFunc(mouse_callback);
  glutMotionFunc(motion_callback);
  glutIdleFunc(idle_callback);
  glutKeyboardUpFunc(keyboard_up_callback);
  glutSpecialUpFunc(special_keyboard_up_callback);
  glutTimerFunc(fps,timer_callback,0);
  glutIgnoreKeyRepeat(1);

  //glut_game_mode_string("640x480:32");
  //glut_enter_game_mode();

  // enter the main loop
  glutMainLoop();

  return 0;
}
