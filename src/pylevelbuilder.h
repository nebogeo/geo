/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "geogame.h"
#include <python2.3/Python.h>

class PyLevelBuilder
{
public:
	static PyLevelBuilder *Get();
	~PyLevelBuilder();

	void Setup(GeoGame *s) { m_Game=s; }
	void Source(const string &filename);
	void DoString(const string &script);

	static dMatrix m_Space;

	static PyObject *Scale(PyObject *self, PyObject* args);
	static PyObject *Translate(PyObject *self, PyObject* args);
	static PyObject *Rotate(PyObject *self, PyObject* args);
	static PyObject *NewGeo(PyObject *self, PyObject* args);

private:
	PyLevelBuilder();
	GeoGame *m_Game;
	static PyLevelBuilder *m_Singleton;
	static PyMethodDef m_Methods[256];
	void BindingHelper(const string &name, PyCFunction func, int flags, const string &doc);
	int m_BindingCount;



};
