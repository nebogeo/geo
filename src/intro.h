/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include <list>
#include <map>
#ifdef WIN32
#include <strstream>
#define istringstream istrstream
#else
#include <sstream>
#endif
#include "geogame.h"
#include "geo.h"

using namespace std;

namespace geogame {

class screen;

class intro
{
public:
	intro(int width, int height, geo_game *game);
	virtual ~intro();

	void init();
	void build(const string &screen, const string &desc);
	void handle(unsigned char key, int button, int state, int x, int y);
	void update();
	void reshape(int width, int height);
	void render();
	bool finished();

	static int m_menu_sound;
	static int m_select_sound;
	static int m_BGsound;
	static int m_BG2sound;

private:
	void init_audio();
	static bool m_init_audio;

	list<geo*> m_geos;

	int m_width,m_height;
	map<string,screen*> m_screens;
	screen* m_current_screen;
	geo_game *m_game;
	bool m_finished;
	bool m_first_time;
	timeval m_last_time;
	float m_deadline;
	double m_time;
	double m_delta;

};

class screen
{
public:
	screen(geo_game *game);
	virtual ~screen() {}

	virtual void handle(unsigned char key, int button, int state, int x, int y);
	virtual void update();
	virtual void render();
	virtual bool result(string& result);
	void set_title(const string &s) { m_title=s; }
	void build(istringstream &s);
	string get_token() { string t=m_token; m_token=""; return t; }


	class widget
	{
	public:
		widget() {}
		virtual bool activated()=0;
		virtual bool handle(unsigned char key, int button, int state, int x, int y)=0;
	    virtual void update()=0;
		virtual void render()=0;
		virtual string get_token() { return ""; }

	private:

	};

	class button : public widget
	{
	public:
		button(const string &name, const string &token, float x, float y);
		virtual ~button() {}

		virtual bool activated();
		virtual bool handle(unsigned char key, int button, int state, int x, int y);
	    virtual void update();
		virtual void render();
		virtual string get_token() { return m_token; }
		void select() { m_selected=true; }
		void previous(button *s) { m_previous=s; }
		void next(button *s) { m_next=s; }

	private:
		string m_name;
		float m_X,m_Y;
		bool m_selected;
		bool m_activated;
		button *m_previous;
		button *m_next;
		string m_token;
	};

	class text : public widget
	{
	public:
		text(const string &text, float x, float y);
		virtual ~text() {}

		virtual bool activated() { return false; }
		virtual bool handle(unsigned char key, int button, int state, int x, int y) { return false; }
	    virtual void update() {}
		virtual void render();

	private:
		float m_X,m_Y;
		string m_text;
	};

protected:

	string m_title;
	geo_game *m_game;
	bool m_finished;
	string m_token;
	list<screen::widget*> m_widgets;
};

}
