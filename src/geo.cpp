/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include <cstdio>
#include "glinc.h"
#include <stdlib.h>
#include "geo.h"
#include "projectiles.h"
#include "utils.h"
#include "feeble.h"
#include "bleep.h"
#include <cmath>

using namespace geogame;

using namespace std;

const unsigned int MAXDESC=2048;
const int MAXGEOS=400;
int geo::geo_count = 0;

static const int GEO_MAX_ENERGY=200;
static const int GEO_FIRE_ENERGY=50;
static const int GEO_FIRE_COST=5;
static const int GEO_TICK_RATE=50;
static const int GEO_TICK_RATE_VAR=50;
static const int MAX_COMPONENT_DEPTH=200;

component::component(const d_matrix &space) :
  m_space(space)
{
  m_pos=space.transform(m_pos);
}

void triangle_component::render()
{
  primitive::get()->triangle();
}

void square_component::render()
{
  primitive::get()->square();
}

void pentagon_component::render()
{
  primitive::get()->pentagon();
}

d_matrix get_matrix()
{
  d_matrix m;
  glGetFloatv(GL_MODELVIEW_MATRIX,(float*)m.m);
  return m;
}

///////////////////////////////////////////////////////////////////////////////////

bool geo::m_init_audio=false;
int geo::m_explosion_sound=0;
int geo::m_death_sound=0;
int geo::m_shot_sound=0;

geo::geo(int generation, const d_matrix &space) :
  m_dirty(true),
  m_space(space),
  m_age(0),
  m_ticks(0),
  m_rate(GEO_TICK_RATE+rand()%GEO_TICK_RATE_VAR),
  m_energy(10),
  m_lifetime(10),
  m_num_triangles(0),
  m_num_squares(0),
  m_just_rebuilt(false),
  m_mass(0)
{
  m_flags|=ENTITYFLAG_ENEMY;
  m_position=space.transform(m_position);

  if (m_position.x<-200 || m_position.x>200 || m_position.y<-200 || m_position.y>200) {
    m_remove_me=true;
  }

  geo_count++;
  m_genome.generation=generation;
  m_genome.score=0;

  if (!m_init_audio) init_audio();
}

geo::~geo()
{
  for (list<component*>::iterator i=m_components.begin(); i!=m_components.end(); i++) {
    delete *i;
  }

  geo_count--;
}

void geo::update(const list<entity*> &world, const d_vector &playerpos)
{
  entity::update(world,playerpos);
  
  m_just_rebuilt=false;
  if (m_dirty) {
    for(list<component*>::iterator i=m_components.begin(); i!=m_components.end(); i++) {
      delete *i;
    }
    m_components.clear();
    
    m_bounding_box.clear();
    
    glPushMatrix();
    glLoadIdentity();
    glMultMatrixf((float*)m_space.m);
    render_part(m_genome.description, 0);
    glPopMatrix();
    
    m_bounding_box.expandby(1.5);
    
    m_dirty=false;
    m_just_rebuilt=true;
  }

  // kill any components we've grown into
  if (m_just_rebuilt) {
    int count=0;
    // should do a bb to bb check first
    for (list<entity*>::const_iterator i=world.begin(); i!=world.end(); i++) {
      if ((*i)->get_flags()==ENTITYFLAG_ENEMY && *i!=this) {
	for(list<component*>::iterator c=m_components.begin(); c!=m_components.end(); c++) {
	  (*i)->check_collide((*c)->get_position(),0.6,true);
	}
      }
      count++;
    }
    
    // update the mass (uses max)
    if (count>m_mass) m_mass=count;
    
    m_num_triangles=0;
    m_num_squares=0;
    m_fire_pos.clear();
    m_birth_pos.clear();
    
    for (list<component*>::iterator i=m_components.begin(); i!=m_components.end(); i++) {
      if ((*i)->num_sides()==3) {
	m_num_triangles++;
	m_fire_pos.push_back((*i)->get_position());
      }
      if ((*i)->num_sides()==4) m_num_squares++;
      if ((*i)->num_sides()==5) m_birth_pos.push_back((*i)->get_space());
    }  
  }

  if (m_ticks%m_rate==0) {
    m_energy += m_num_squares;
    if (m_energy<GEO_MAX_ENERGY) process();
    m_age++;
    
    // if player in range
    if (m_energy>GEO_FIRE_ENERGY && playerpos.distsq(m_position)<30*30) {
      for(vector<d_vector>::iterator i=m_fire_pos.begin(); i!=m_fire_pos.end(); i++) {
	if (m_energy>GEO_FIRE_ENERGY) {
	  bleep::get()->play(m_shot_sound, 220, 1);
	  d_vector jitter(rand_range(-5,5),rand_range(-5,5),rand_range(-5,5));
	  rocket *newrocket = new rocket(*i,playerpos+jitter,rand_range(0.2,1));
	  newrocket->set_explode_sound(m_explosion_sound);
	  m_new_entities.push_back(newrocket);
	  m_energy-=GEO_FIRE_COST;
	  //m_genome.score++;
	}
      }
    }
    
    if (m_energy==0) m_remove_me=true;
    
    if(m_energy>GEO_MAX_ENERGY && geo_count<MAXGEOS) {
      bleep::get()->play(m_death_sound, 220, 1);
      
      // give birth
      for(vector<d_matrix>::iterator i=m_birth_pos.begin(); i!=m_birth_pos.end(); i++) {
	geo *seed = new geo(++m_genome.generation, *i);
	seed->m_genome.axiom=m_genome.axiom;
	seed->m_genome.rules=m_genome.rules;
	seed->mutate(0.01);
	cerr<<"axiom:"<<seed->m_genome.axiom<<endl;
	for (vector<pair<char,string> >::iterator ri=m_genome.rules.begin(); ri!=m_genome.rules.end(); ++ri) {
	  cerr<<"rules:"<<ri->first<<" -> "<<ri->second<<endl;
	}
	seed->m_genome.description=m_genome.axiom;
	m_new_entities.push_back(seed);
	// encourage size
	m_genome.score+=sqrt((m_bounding_box.max.x-m_bounding_box.min.x) *
			     (m_bounding_box.max.y-m_bounding_box.min.y)) /
	  geo_count;
      }
      m_remove_me=true;
    }
    
    if (m_components.empty()) m_remove_me=true;
  }
  
  m_ticks++;
}

void geo::render_part(const string &part, int depth)
{
  if (depth>=MAX_COMPONENT_DEPTH) return;

  //cerr<<"render_part: "<<part<<endl;
  // parse string
  int numsides=0;
  unsigned int pos=0;
  component *newcomp;
  d_matrix m=get_matrix();

  bool done=false;
  while (!done) {
    switch (part[pos]) {
    case 't': newcomp = new triangle_component(m); done=true; break;
    case 's': newcomp = new square_component(m); done=true; break;
    case 'p': newcomp = new pentagon_component(m); done=true; break;
      break;
    default: break; // invalid or malformed, or containing an lsystem remmnant
    }
    pos++;
    if (pos>part.size()) return; // no usable sections found
  }

  d_vector zero(0,0,0);
  zero = m.transform(zero);
  m_bounding_box.expand(zero);
  numsides=newcomp->num_sides();
  m_components.push_back(newcomp);

  pos=1;
  int side=1;
  while(pos<part.size() && side<numsides) {
    string branch=extract_branch(part,pos);
    //cerr<<"extract_branch: "<<part<<" "<<pos<<": "<<branch<<endl;
    if (branch!="") {
      glPushMatrix();
      glRotatef(180+(side*(360.0/(float)numsides)),0,0,1);
      glTranslatef(0,0.6,0);
      //glScalef(0.9,0.9,0.9);
      render_part(branch,++depth);
      glPopMatrix();
    }
    side++;
  }
}

void geo::render()
{
  for(list<component*>::iterator i=m_components.begin(); i!=m_components.end(); i++) {
    glPushMatrix();
    glMultMatrixf((float*)(*i)->get_space().m);
    (*i)->render();
    glPopMatrix();
  }

  /*  glPushMatrix();
  glMultMatrixf((float*)m_space.m);
  glColor4f(1,1,1,0.5);
  glScalef(0.4,0.4,0.4);
  char info[256];
  sprintf(info,"age:%d gen: %d energy:%d life:%d",
	  m_age,m_genome.generation,m_energy,m_lifetime);
  //draw_text(info);
  glTranslatef(-0.4,-0.2,0);
  draw_text(m_genome.axiom);
  glTranslatef(0,-0.5,0);
  draw_text(string("0 -> ")+m_genome.rules[0].second);
  glTranslatef(0,-0.5,0);
  draw_text(string("1 -> ")+m_genome.rules[1].second);
  glTranslatef(0,-0.5,0);
  draw_text(string("2 -> ")+m_genome.rules[2].second);
  glTranslatef(0,-0.5,0);
  draw_text(string("3 -> ")+m_genome.rules[3].second);
  glTranslatef(0,-0.5,0);
  //glScalef(0.4,0.4,0.4);
  //draw_text(m_genome.description);
  glPopMatrix();*/
}

string geo::extract_branch(const string &ls, unsigned int &pos) // extracts bit between []
{
  int dcount=-1;
  string::const_iterator i=ls.begin();
  for (unsigned int n=0; n<pos; n++) i++;
  // search for a branch start
  while (*i!='[' && i!=ls.end()) i++;
  string::const_iterator start=i;
  start++;
  pos++;

  while(i!=ls.end()) {
    if (dcount==0 && (*i==']')) {
      return string(start,i);
    }
    if (*i=='[') dcount++;
    if (*i==']') dcount--;
    i++;
    pos++;
  }

  //cerr<<"invalid"<<endl;
  // the structure is technically invalid, but we'll just return nothing
  // to make it less brittle (makes mutation much easier) - basically just
  // ignores the ['s with no corresponding ]
  return string("");
}

void geo::process()
{
  m_dirty=true;
  string temp,replacment;
  string::iterator i=m_genome.description.begin();

  while(i!=m_genome.description.end()) {
      replacment=*i;
      for (vector<pair<char,string> >::iterator r=m_genome.rules.begin(); r!=m_genome.rules.end(); r++) {
	if (*i==r->first) {
	  replacment=r->second;
	}
      }
      temp+=replacment;
      if (temp.size()>MAXDESC) break;
      i++;
  }
  //cerr<<temp<<endl;
  m_genome.description=temp;
}

string geo::get_fragment()
{
  switch(rand()%29) {
  case 0: return "[";
  case 1: return "]";
  case 2: return "t";
  case 3: return "s";
  case 4: return "p";
  case 5: return "0";
  case 6: return "1";
  case 7: return "2";
  case 8: return "3";
  case 9: return "[";
  case 10: return "]";
  case 11: return "[";
  case 12: return "]";
  case 13: return "[";
  case 14: return "]";
  case 15: return "[";
  case 16: return "]";
  case 17: return "t";
  case 18: return "s";
  case 19: return "p";
  case 20: return "t";
  case 21: return "s";
  case 22: return "p";
  }
  return "";
}

void geo::randomise()
{
  m_dirty=true;
	
  /*m_genome.axiom="p[0][0][0][0]";
  m_genome.rules.push_back(pair<char,string>('0',"s[][1][]"));
  m_genome.rules.push_back(pair<char,string>('1',"t[][2]"));
  m_genome.rules.push_back(pair<char,string>('2',"p[][3]"));
  m_genome.rules.push_back(pair<char,string>('3',"t[0][1]"));
  m_genome.description=m_genome.axiom;
  */	
  /*m_genome.axiom="0";
    m_genome.rules.push_back(pair<char,string>('0',"s[1][s[][0][2]][]"));
    m_genome.rules.push_back(pair<char,string>('1',"t[][]"));
    m_genome.rules.push_back(pair<char,string>('2',"s[][3][]"));
    m_genome.rules.push_back(pair<char,string>('3',"p"));
    m_genome.description=m_genome.axiom;
  */

  /*  m_genome.axiom="[[sp3ss";
  m_genome.rules.push_back(pair<char,string>('0',"][p3t]]]]"));
  m_genome.rules.push_back(pair<char,string>('1',"t][[[[["));
  m_genome.rules.push_back(pair<char,string>('2',"2[s]t[tt"));
  m_genome.rules.push_back(pair<char,string>('3',"ps[spp0s0"));
  m_genome.description=m_genome.axiom;
  */
    	
  int desclength=10;
  m_genome.axiom="";
  for(int n=0; n<desclength; n++) {
    m_genome.axiom+=get_fragment();
  }
  m_genome.description=m_genome.axiom;
  
  m_genome.rules.clear();
  int numrules=4;
  for(int r=0; r<numrules; r++) {
    int rulelength=10;
    string rule="";
    for(int n=0; n<desclength; n++) rule+=get_fragment();
    char temp[3];
    sprintf(temp,"%d",r);
    m_genome.rules.push_back(pair<char,string>(temp[0],rule));
  }
}

string geo::mutate_helper(string s, float rate)
{
  string copy;
  for (unsigned int i=0; i<s.size(); i++) {
    if (rand_float()<rate) copy+=get_fragment(); // sometimes insert something new
    if (rand_float()>rate) copy+=s[i]; // sometimes forget to copy old part
  }
  return copy;
}

void geo::mutate(float rate)
{
  m_genome.axiom=mutate_helper(m_genome.axiom,rate);
  m_genome.description=mutate_helper(m_genome.description,rate);
  for (vector<pair<char,string> >::iterator i=m_genome.rules.begin(); i!=m_genome.rules.end(); i++) {
    i->second=mutate_helper(i->second,rate);
  }
  process();
}

bool geo::check_collide(const d_vector &pos, float radius, bool damage)
{
  if (!m_bounding_box.inside(pos,radius)) return false;

  for(list<component*>::iterator i=m_components.begin(); i!=m_components.end(); i++) {
    if ((*i)->get_position().distsq(pos)<(1+radius)*(1+radius)) {
      if (damage) {
	delete *i;
	m_components.erase(i);
      }
      return true;
    }
  }
  return false;
}

void geo::init_audio()
{
  m_init_audio=true;

  feeble::time_length=5;
  feeble::samplerate=22050;

  feeble::time_length=2;
  feeble::value volvalue(0.1);
  feeble::mult volmult;

  // geo explosion sound
  {
    feeble::time_length=0.5;
    feeble::envelope env(feeble::envelope::HILL);
    feeble::mult envmult;
    feeble::value envmultvalue(50);
    feeble::generator osc(feeble::generator::NOISE);

    envmult.patch("a",&envmultvalue);
    envmult.patch("b",&env);
    osc.patch("freq",&envmult);
    volmult.patch("a",&osc);
    volmult.patch("b",&volvalue);
    volmult.execute();

    m_explosion_sound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
  }

  // die sound
  {
    feeble::time_length=0.1;
    feeble::envelope env(feeble::envelope::RAMPUP);
    feeble::mult envmult;
    feeble::value envmultvalue(1000);
    feeble::generator osc(feeble::generator::SINE);

    envmult.patch("a",&envmultvalue);
    envmult.patch("b",&env);
    osc.patch("freq",&envmult);
    volmult.patch("a",&osc);
    volmult.patch("b",&volvalue);
    volmult.execute();

    m_death_sound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
  }

  // shot sound
  {
    feeble::time_length=0.5;
    feeble::envelope env(feeble::envelope::RAMPUP);
    feeble::mult envmult;
    feeble::value envmultvalue(2000);
    feeble::generator osc(feeble::generator::SINE);

    envmult.patch("a",&envmultvalue);
    envmult.patch("b",&env);
    osc.patch("freq",&envmult);
    volmult.patch("a",&osc);
    volmult.patch("b",&volvalue);
    volmult.execute();

    m_shot_sound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
  }

}
