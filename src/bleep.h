// simplified openal playback of sampledata

#include <AL/al.h>
#include <AL/alc.h>
//#include <AL/alext.h>
#include <AL/alut.h>

namespace geogame {

  const static int MAX_SOURCES = 10;

  class bleep
  {
  public:
    static bleep *get();

    int register_sound(short *data, unsigned int size);
    int register_sound(float *data, unsigned int size);
    // loop refers the the looping channel to use
    int play(int id, float freq, float vol, int loop=0);
    void modify(int pid, float freq, float vol);
    void stop_loop(int loopid);

    ~bleep();
  private:
    bleep();

    ALCdevice *m_device;
    ALCcontext *m_context;
    unsigned int m_sources[MAX_SOURCES];
    unsigned int m_looping_sources[MAX_SOURCES];
    int m_current_source;
    static bleep *m_singleton;
  };
}
