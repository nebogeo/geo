/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "glinc.h"
#include "utils.h"
#include "dada.h"

using namespace geogame;

void geogame::draw_text(const string &text)
{
	glPushMatrix();
	glScalef(0.003,0.003,0.003);
	glTranslatef(0,0,-100);
	//gl_enable(GL_LINE_SMOOTH);
	int count=0,line=0;
	for (unsigned int n=0; n<text.length(); n++)
	{
		glutStrokeCharacter(GLUT_STROKE_ROMAN, text[n]);

		if (count>80)
		{
			line++;
			//gl_raster_pos3f(0.0, (-line)/10.0f, -1.1);
			count=0;
		}

		count++;
	}
	//gl_disable(GL_LINE_SMOOTH);
	glPopMatrix();
}

//////////////////////////////////////////////////////////

primitive *primitive::m_singleton=NULL;

primitive::primitive()
{
	float radius;

	m_triangle = glGenLists(1);
	glNewList(m_triangle,GL_COMPILE);
		glColor4f(1,0,0,0.5);
		radius=0.55;
		glBegin(GL_TRIANGLES);
		glVertex3f(sin(0.0*DEG_CONV)*radius,cos(0.0*DEG_CONV)*radius,0);
		glVertex3f(sin(120.0*DEG_CONV)*radius,cos(120.0*DEG_CONV)*radius,0);
		glVertex3f(sin(240.0*DEG_CONV)*radius,cos(240.0*DEG_CONV)*radius,0);
		glEnd();
	glEndList();

	m_square = glGenLists(1);
	glNewList(m_square,GL_COMPILE);
		glColor4f(0,0,1,0.5);
		radius=0.3;
		glBegin(GL_QUADS);
		glVertex3f(radius,radius,0);
		glVertex3f(-radius,radius,0);
		glVertex3f(-radius,-radius,0);
		glVertex3f(radius,-radius,0);
		glEnd();
	glEndList();

	m_scanner_key = glGenLists(1);
	glNewList(m_scanner_key,GL_COMPILE);
		radius=1;
		glBegin(GL_QUADS);
		glVertex3f(radius,radius,0);
		glVertex3f(-radius,radius,0);
		glVertex3f(-radius,-radius,0);
		glVertex3f(radius,-radius,0);
		glEnd();
	glEndList();


	m_pentagon = glGenLists(1);
	glNewList(m_pentagon,GL_COMPILE);
		glColor4f(1,0,1,0.5);
		radius=0.35;
		glBegin(GL_POLYGON);
		glVertex3f(sin(0.0*DEG_CONV)*radius,cos(0.0*DEG_CONV)*radius,0);
		glVertex3f(sin(72.0*DEG_CONV)*radius,cos(72.0*DEG_CONV)*radius,0);
		glVertex3f(sin(144.0*DEG_CONV)*radius,cos(144.0*DEG_CONV)*radius,0);
		glVertex3f(sin(216.0*DEG_CONV)*radius,cos(216.0*DEG_CONV)*radius,0);
		glVertex3f(sin(288.0*DEG_CONV)*radius,cos(288.0*DEG_CONV)*radius,0);
		glEnd();
	glEndList();

	m_particle = glGenLists(1);
	glNewList(m_particle,GL_COMPILE);
		radius=0.35;
		glBegin(GL_POLYGON);
		glVertex3f(sin(0.0*DEG_CONV)*radius,cos(0.0*DEG_CONV)*radius,0);
		glVertex3f(sin(72.0*DEG_CONV)*radius,cos(72.0*DEG_CONV)*radius,0);
		glVertex3f(sin(144.0*DEG_CONV)*radius,cos(144.0*DEG_CONV)*radius,0);
		glVertex3f(sin(216.0*DEG_CONV)*radius,cos(216.0*DEG_CONV)*radius,0);
		glVertex3f(sin(288.0*DEG_CONV)*radius,cos(288.0*DEG_CONV)*radius,0);
		glEnd();
	glEndList();

	m_key = glGenLists(1);
	glNewList(m_key,GL_COMPILE);
		radius=1;
		glBegin(GL_LINE_LOOP);
		glVertex3f(sin(0.0*DEG_CONV)*radius,cos(0.0*DEG_CONV)*radius,0);
		glVertex3f(sin(120.0*DEG_CONV)*radius,cos(120.0*DEG_CONV)*radius,0);
		glVertex3f(sin(240.0*DEG_CONV)*radius,cos(240.0*DEG_CONV)*radius,0);
		glEnd();
	glEndList();

	m_explosion = glGenLists(1);
	glNewList(m_explosion,GL_COMPILE);
		radius=1;
		glBegin(GL_POLYGON);
		for(int s=0; s<=12; s++)
		{
			glVertex3f(sin((360/12.0f)*s*DEG_CONV),
			           cos((360/12.0f)*s*DEG_CONV),0);
		}
		glEnd();
		glPopMatrix();
	glEndList();

	m_weapon_pickup = glGenLists(1);
	glNewList(m_weapon_pickup,GL_COMPILE);
		radius=0.35;
		glBegin(GL_LINE_LOOP);
		glVertex3f(sin(0.0*DEG_CONV*2)*radius,cos(0.0*DEG_CONV*2)*radius,0);
		glVertex3f(sin(72.0*DEG_CONV*2)*radius,cos(72.0*DEG_CONV*2)*radius,0);
		glVertex3f(sin(144.0*DEG_CONV*2)*radius,cos(144.0*DEG_CONV*2)*radius,0);
		glVertex3f(sin(216.0*DEG_CONV*2)*radius,cos(216.0*DEG_CONV*2)*radius,0);
		glVertex3f(sin(288.0*DEG_CONV*2)*radius,cos(288.0*DEG_CONV*2)*radius,0);
		glEnd();
	glEndList();

}

primitive::~primitive()
{
}

primitive *primitive::get()
{
	if (!m_singleton) m_singleton=new primitive;
	return m_singleton;
}
