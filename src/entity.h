/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include <list>
#include "dada.h"

using namespace std;

#define ENTITYFLAG_ENEMY		       0x01
#define ENTITYFLAG_PROJECTILE	       0x02
#define ENTITYFLAG_EXPLOSION	       0x04
#define ENTITYFLAG_KEY			       0x08
#define ENTITYFLAG_CAUGHTKEY	       0x10
#define ENTITYFLAG_PARTICLES	       0x20
#define ENTITYFLAG_WEAPONPICKUP	       0x40
#define ENTITYFLAG_CAUGHT_WEAPONPICKUP 0x80

#ifndef ENTITY
#define ENTITY

namespace geogame {

class entity
{
public:
	entity() : m_flags(0), m_remove_me(false) {}
	virtual ~entity() {}

	virtual void update(const list<entity*> &world, const d_vector &playerpos);
	virtual void render()=0;
	virtual const d_vector &get_position()=0;
	virtual float get_mass() { return 0; }
	virtual bool check_collide(const d_vector &pos, float radius, bool damage=false)=0;

	bool remove_me() { return m_remove_me; }
	const list<entity*> &get_new_entities() { return m_new_entities; }
	int get_flags() { return m_flags; }
	const d_bounding_box& get_bounding_box() { return m_bounding_box; }

protected:

	d_bounding_box m_bounding_box;
	int m_flags;
	bool m_remove_me;
	list<entity*> m_new_entities;
};
}
#endif
