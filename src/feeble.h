/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

// an experimental ultra low quality, game synth renderer
// for making bloops, bleeps and jitters

#include <map>
#include <string>

using namespace std;

namespace geogame {

class feeble
{
public:
	feeble();
	~feeble();

	static unsigned int samplerate;
	static float time_length;

	class module
	{
	public:
		module();
		virtual ~module();

		virtual void patch(const string &input, module *p) { m_inputs[input]=p; }
		virtual float execute();
		virtual float get(unsigned int n);
		float *get_buffer() { return m_buffer; }
		unsigned int get_size() { return m_size; }
		float get_input(const string &name, unsigned int n);
		void dump();

	protected:

		map<string,module*> m_inputs;
		float *m_buffer;
		unsigned int m_size;
		unsigned int m_samplerate;
		float m_time_length;
	};

	class value : public module
	{
	public:
		value(float value) : m_value(value) {}
		~value() {}
		virtual float execute();
		float get(unsigned int n) { return m_value; }
	private:
		float m_value;
	};

	class mult : public module
	{
	public:
		virtual float execute();
	private:
	};

	class add : public module
	{
	public:
		virtual float execute();
	private:
	};


	class generator : public module
	{
	public:
		enum type{SINE,SQUARE,TRIANGLE,SAWTOOTH,REVSAW,NOISE};
		generator(type t);
		~generator() {}

		virtual float execute();
	private:

		type m_type;
	};

	class envelope : public module
	{
	public:
		enum type{RAMPUP,RAMPDOWN,HILL,VALLEY};
		envelope(type t);
		virtual ~envelope() {}
		virtual float execute();

	private:
		type m_type;
	};

private:
};
}
