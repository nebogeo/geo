/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "dada.h"
#include "entity.h"

#ifndef PROJECTILE_ENTITY
#define PROJECTILE_ENTITY

namespace geogame {

class projectile : public entity
{
public:
	projectile() {}
	projectile(const d_vector &pos, const d_vector &vec, const d_vector &inr);
	virtual ~projectile();
	virtual void update(const list<entity*> &world, const d_vector &playerpos);
	virtual bool check_collide(const d_vector &pos, float radius, bool damage=false) { return false; }
	virtual const d_vector &get_position() { return m_pos; }

	const d_vector &get_vec() { return m_vec; }

protected:

	d_vector m_pos;
	d_vector m_vec;
	d_vector m_inertia;
	int 	m_age;
};

class bullet : public projectile
{
public:
	bullet(const d_vector &pos, const d_vector &vec, const d_vector &inr);
	virtual void render();
	virtual void update(const list<entity*> &world, const d_vector &playerpos);
};

class missile : public projectile
{
public:
	missile(const d_vector &pos, const d_vector &vec, const d_vector &inr);
	virtual void render();
	virtual void update(const list<entity*> &world, const d_vector &playerpos);
	void set_explode_sound(int s) { m_explode_sound=s; }
private:
	bool m_exploding;
	float m_fade;
	int m_explode_sound;
};

class rocket : public projectile
{
public:
	rocket(const d_vector &pos, const d_vector &aimpos, float speed);
	virtual void render();
	virtual void update(const list<entity*> &world, const d_vector &playerpos);
	virtual bool check_collide(const d_vector &pos, float radius, bool damage=false);
	void set_explode_sound(int s) { m_explode_sound=s; }

private:
	bool m_exploding;
	float m_fade;
	d_vector m_aim_pos;
	float m_speed;
	int m_explode_sound;
};

class mutator : public projectile
{
public:
	mutator(const d_vector &pos, const d_vector &vec, const d_vector &inr);
	virtual void render();
	virtual void update(const list<entity*> &world, const d_vector &playerpos);
	void set_explode_sound(int s) { m_explode_sound=s; }

private:
	bool m_exploding;
	float m_size;
	int m_explode_sound;
};
}
#endif
