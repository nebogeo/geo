/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include <list>
#include "entity.h"

#ifndef PARTICLES
#define PARTICLES

namespace geogame {

class particles: public entity
{
public:
	particles(int maxparticles);
	~particles();

	class particle
	{
	public:
		particle(const d_vector &p,const d_vector &d,const d_vector &i);
		d_vector position;
		d_vector direction;
		d_vector inertia;
		int age;
	};

	virtual void update(const list<entity*> &world, const d_vector &playerpos);
	virtual void render();
	virtual const d_vector &get_position() { return m_position; }
	virtual bool check_collide(const d_vector &pos, float radius, bool damage=false) { return false; }

	void add_particle(const particle &p);

private:
	d_vector m_position;
	int m_max_particles;
	list<particle> m_particles;
};
}
#endif
