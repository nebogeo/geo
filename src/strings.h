/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include <string>

using namespace std;

static const string TODO_SCR="title [todo list (temp)]\
					text [* HUD work]\
					text [* points scoring?]\
					text [* more audio]\
					text [* 4 more weapons needed (quarantine thing)]\
					text [* windows fixes - static link]\
					text [* finish info text + display graphics explanation]\
					text [* improve recharging (just on blue components?)]\
					text [* splash screen while generating audio]\
					gap \
					button [exit to main menu] [title]";


static const string TITLE_SCR="title [geo]\
				  button [enter game] [finished]\
				  button [information] [info1]\
				  button [about] [about]\
				  button [todo list] [todo]\
				  button [exit] [exit]";

static const string ABOUT_SCR="title [about geo]\
					text [geo is a game written by dave griffiths]\
					text [dave@pawfal.org]\
					gap \
					text [version 0.2]\
					gap \
					button [back] [title]";

static const string INFO_SCR1="title [information (temp)]\
					text [objectives]\
					gap \
					text [collect all the trianular power items to progress]\
					text [through the levels. you'll also need to pick up the star]\
					text [shaped powerups to give you better weapons to use.]\
					text [fly through these entities to pick them up.]\
					gap \
					text [during play, and from level to level, the \"geo\" entities]\
					text [will evolve to multiply and hamper your progress.]\
					text [they create a gravitational force on your ship, so be careful]\
					text [of large colonies of the geo - your engines may not be able to]\
					text [achieve escape velocity in some cases.]\
					gap \
					button [next] [info2]\
					button [exit to main menu] [title]";

static const string INFO_SCR2="title [information (temp)]\
					text [controlling the ship and weapons]\
					gap \
					text [use the wasd keys to steer and activate thrusters.]\
					text [use the number keys to select weapons (as you collect them)]\
					text [space bar fire the selected weapon]\
					gap \
					text [w/f : windowed/fullscreen mode] \
					text [+/- : zoom display] \
					text [p : pause/unpause] \
					text [q : exit game] \
					gap \
					text [energy can be recharged by landing on the geos] \
					gap \
					button [next] [info3]\
					button [back] [info1]\
					button [exit to main menu] [title]";

static const string INFO_SCR3="title [information (temp)]\
					text [the scanner]\
					text [the green dots in the background represent the scanner's]\
					text [view of your environment, the small dots are geo's, the]\
					text [larger dots are the energy items for collecting.]\
					gap \
					button [back] [info2]\
					button [exit to main menu] [title]";
