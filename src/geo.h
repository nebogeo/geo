/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "dada.h"
#include "entity.h"
#include <string>
#include <vector>

#ifndef GEO
#define GEO

namespace geogame {

  class component
  {
  public:
    component(const d_matrix &space);
    virtual ~component() {}

    virtual void render()=0;
    virtual int num_sides()=0;
    const d_matrix &get_space() { return m_space; }
    d_vector get_position() { return m_pos; }

  private:
    d_matrix m_space;
    d_vector m_pos; // for convienience
  };

  class triangle_component : public component
  {
  public:
  triangle_component(const d_matrix &space) : component(space) {}
    virtual ~triangle_component() {}
    virtual void render();
    virtual int num_sides() { return 3; }
  };

  class square_component : public component
  {
  public:
  square_component(const d_matrix &space) : component(space) {}
    virtual ~square_component() {}
    virtual void render();
    virtual int num_sides() { return 4; }
  };

  class pentagon_component : public component
  {
  public:
  pentagon_component(const d_matrix &space) : component(space) {}
    virtual ~pentagon_component() {}
    virtual void render();
    virtual int num_sides() { return 5; }
  };

  /////////////////////////////////////////////////////////////////////////////////

  class geo : public entity
  {
  public:
    geo(int generation, const d_matrix &space);
    virtual ~geo();

    virtual void update(const list<entity*> &world, const d_vector &playerpos);
    virtual void render();
    virtual bool check_collide(const d_vector &pos, float radius, bool damage=false);
    virtual float get_mass() { return m_mass; }
    virtual const d_vector &get_position() { return m_position; }

    void process();
    void randomise();
    void mutate(float rate);

    class genome
    {
    public:
      string description;
      string axiom;
      vector<pair<char,string> > rules;
      int generation;
      float score;
      bool operator<(const genome &other) { return score<other.score; }
    };

    genome m_genome;

    static int geo_count;

  private:
    void render_part(const string &part, int depth);
    string extract_branch(const string &ls, unsigned int &pos);
    string get_fragment();
    string mutate_helper(string s, float rate);

    static void init_audio();
    static bool m_init_audio;
    static int m_explosion_sound;
    static int m_death_sound;
    static int m_shot_sound;

    bool m_dirty;

    list<component*> m_components;
    vector<d_matrix> m_birth_pos;
    vector<d_vector> m_fire_pos;

    d_matrix m_space;
    d_vector m_position;

    int m_age;
    int m_ticks;
    int m_rate;
    int m_energy;
    int m_lifetime;
    int m_num_triangles;
    int m_num_squares;
    bool m_just_rebuilt;
    int m_mass;
  };
}
#endif
