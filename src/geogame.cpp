/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include <cstdio>
#include <unistd.h>
#include "glinc.h"
#include "geogame.h"
#include "key.h"
#include "projectiles.h"
#include "utils.h"
#include "feeble.h"
#include "bleep.h"

using namespace geogame;

static const float MAX_GRAV_DIST=30;
static const float EXP_DAMAGE_MULT=10;
static const float EXP_PUSH_MULT=0.3;
static const int LEVELCOMPLETE_DELAY=500;
static const int LIFELOST_DELAY=500;
static const int ENDGAME_DELAY=500;

geo_game::geo_game(int W, int H, char *n) :
m_rotX(0),
m_rotY(0),
m_posX(0),
m_posY(0),
m_disY(0),
m_width(W),
m_height(H),
m_scale(0.5),
m_weapon_recharge(0),
m_player_dir(0,1,0),
m_player_vec(0,0,0),
m_first_time(true),
m_player_health(100.0),
m_level(0),
m_lives(5),
m_finished(false),
m_level_complete(false),
m_delay(0),
m_flash_counter(0),
m_recharging(0),
m_weapon_level(1),
m_current_weapon(0),
m_frame(0),
m_display_text_frames(-1),
m_pause_mode(false),
m_deadline(1/25.0f),
m_time(0),
m_delta(0)
{

	// init the audio
	init_audio();
}

void geo_game::init()
{
	m_pressed_keys.clear();
	m_BGsoundID = bleep::get()->play(m_BGsound, 220, 1, 1);
	m_player_dir=d_vector(0,1,0);
	m_player_vec=d_vector(0,0,0);
	m_player_pos=d_vector(0,0,0);
	m_level=1;
	m_player_health=100.0;
	m_lives=5;
	m_finished=false;
	m_level_complete=false;
	clear();
	m_store.clear();
	m_delay=0;
	m_weapon_level=1;
	m_current_weapon=0;
	for (int i=0; i<10; i++) m_weapon_energy[i]=1;
	make_new_level();
}

void geo_game::clear()
{
	for (list<entity*>::iterator i=m_world.begin(); i!=m_world.end(); i++)
	{
		delete *i;
	}
	m_world.clear();
}

void geo_game::reshape(int width, int height)
{
	m_width=width;
	m_height=height;
	m_first_time=true;
}

void geo_game::handle(unsigned char key, int button, int state, int x, int y)
{
	if (key && state==0) m_pressed_keys.insert(key); // keydown
	if (key && state==1) m_pressed_keys.erase(key); // keyup
	m_last_mouseX=x;
	m_last_mouseY=y;

	if (key=='q')
	{
		m_finished=true;
		clear();
		bleep::get()->stop_loop(1);
	}

}

void geo_game::update()
{
	if (m_pressed_keys.find('p')!=m_pressed_keys.end())
	{
		m_pressed_keys.clear();
		m_pause_mode=!m_pause_mode;
	}
	if (m_pause_mode) return;

	if (m_pressed_keys.find('a')!=m_pressed_keys.end())
	{
		d_matrix m;
		m.rotz(5);
		m_player_dir=m.transform(m_player_dir);
	}

	if (m_pressed_keys.find('d')!=m_pressed_keys.end())
	{
		d_matrix m;
		m.rotz(-5);
		m_player_dir=m.transform(m_player_dir);
	}

	if (m_pressed_keys.find('w')!=m_pressed_keys.end())
	{
		m_player_vec+=m_player_dir*0.007;
		d_vector dir(-m_player_dir.x*rand_range(0,0.02),
					-m_player_dir.y*rand_range(0,0.02),
					-m_player_dir.z*rand_range(0,0.02));
		if (m_thrust_particles) m_thrust_particles->add_particle(particles::particle(m_player_pos,dir,m_player_vec));
	}

	if (m_pressed_keys.find('s')!=m_pressed_keys.end())
	{
		m_player_vec-=m_player_dir*0.007;
	}

	if (m_pressed_keys.find(' ')!=m_pressed_keys.end())
	{
		if (m_weapon_energy>0)
		{
			m_weapon_energy[m_current_weapon]-=0.005;
			switch (m_current_weapon)
			{
				case 1:
					if (m_weapon_recharge*m_weapon_energy[1]>4)
					{
						m_weapon_recharge=0;
						bleep::get()->play(m_fire_sound, 220, 1);
						m_world.push_back(new bullet(m_player_pos,m_player_dir*0.4,m_player_vec));
					}
				break;

				case 2:
				{
					if (m_weapon_recharge*m_weapon_energy[2]>2)
					{
						m_weapon_recharge=0;
						bleep::get()->play(m_fire_sound, 220, 1);
						float angle=sin(m_frame*0.2)*0.2;
						if (m_player_dir.y>0) angle+=atan(m_player_dir.x/m_player_dir.y);
						else angle+=atan(m_player_dir.x/m_player_dir.y)+3.141; // oh, this is nice...
						d_vector dir(sin(angle),cos(angle),0);
						m_world.push_back(new bullet(m_player_pos,dir*0.25,m_player_vec));
					}
				}
				break;

				case 3:
					if (m_weapon_recharge*m_weapon_energy[3]>10)
					{
						m_weapon_recharge=0;
						bleep::get()->play(m_fire_sound, 220, 1);
						missile *newmissile=new missile(m_player_pos,m_player_dir*0.4,m_player_vec);
						newmissile->set_explode_sound(m_explosion_sound);
						m_world.push_back(newmissile);
					}
				break;

				case 4:
				{
					if (m_weapon_recharge*m_weapon_energy[4]>0.5)
					{
						m_weapon_energy[4]-=0.02;
						m_weapon_recharge=0;
						bleep::get()->play(m_fire_sound, 220, 1);
						float angle=m_frame*0.2;
						if (m_player_dir.y>0) angle+=atan(m_player_dir.x/m_player_dir.y);
						else angle+=atan(m_player_dir.x/m_player_dir.y)+3.141; // oh, this is nice...
						d_vector dir(sin(angle),cos(angle),0);
						m_world.push_back(new bullet(m_player_pos,dir*0.25,m_player_vec));
					}
				}
				break;

				case 5:
					if (m_weapon_recharge*m_weapon_energy[5]>5)
					{
						m_weapon_recharge=0;
						bleep::get()->play(m_fire_sound, 220, 1);
						float angle=sin(m_frame*0.2)*0.3;
						if (m_player_dir.y>0) angle+=atan(m_player_dir.x/m_player_dir.y);
						else angle+=atan(m_player_dir.x/m_player_dir.y)+3.141; // oh, this is nice...
						d_vector dir(sin(angle),cos(angle),0);
						missile *newmissile=new missile(m_player_pos,dir*0.25,m_player_vec);
						newmissile->set_explode_sound(m_explosion_sound);
						m_world.push_back(newmissile);
					}
				break;

				case 6:
					if (m_weapon_recharge*m_weapon_energy[6]>20)
					{
						m_weapon_recharge=0;
						mutator *newmissile=new mutator(m_player_pos,m_player_dir*0.1,m_player_vec);
						newmissile->set_explode_sound(m_mute_sound);
						m_world.push_back(newmissile);
					}
				break;

            case 7:
					if (m_weapon_recharge*m_weapon_energy[5]>2)
					{
						m_weapon_recharge=0;
						bleep::get()->play(m_fire_sound, 120, 1);
						float angle=sin(m_frame*0.2)*0.3;
						if (m_player_dir.y>0) angle+=atan(m_player_dir.x/m_player_dir.y);
						else angle+=atan(m_player_dir.x/m_player_dir.y)+3.141; // oh, this is nice...
						d_vector dir(sin(angle),cos(angle),0);
						missile *newmissile=new missile(m_player_pos,dir*0.25,m_player_vec);
						newmissile->set_explode_sound(m_explosion_sound);
						m_world.push_back(newmissile);
					}
				break;


			}
		}
	}

	int selectedweapon=99;
	if (m_pressed_keys.find('1')!=m_pressed_keys.end()) selectedweapon=1;
	if (m_pressed_keys.find('2')!=m_pressed_keys.end()) selectedweapon=2;
	if (m_pressed_keys.find('3')!=m_pressed_keys.end()) selectedweapon=3;
	if (m_pressed_keys.find('4')!=m_pressed_keys.end()) selectedweapon=4;
	if (m_pressed_keys.find('5')!=m_pressed_keys.end()) selectedweapon=5;
	if (m_pressed_keys.find('6')!=m_pressed_keys.end()) selectedweapon=6;
	if (m_pressed_keys.find('7')!=m_pressed_keys.end()) selectedweapon=7;
	if (m_pressed_keys.find('8')!=m_pressed_keys.end()) selectedweapon=8;
	if (m_pressed_keys.find('9')!=m_pressed_keys.end()) selectedweapon=9;

	for (int i=0; i<10; i++)
	{
		m_weapon_energy[i]+=0.003;
		if (m_weapon_energy[i]>1) m_weapon_energy[i]=1;
	}

	m_weapon_recharge++;
	if (selectedweapon<=m_weapon_level) m_current_weapon=selectedweapon;

	if (m_pressed_keys.find('=')!=m_pressed_keys.end())
	{
		m_scale*=1.01;
	}

	if (m_pressed_keys.find('-')!=m_pressed_keys.end())
	{
		m_scale*=0.99;
	}

	if (m_pressed_keys.find('c')!=m_pressed_keys.end())
	{
		for (list<entity*>::iterator i=m_world.begin(); i!=m_world.end(); i++)
		{
			if ((*i)->get_flags()&ENTITYFLAG_ENEMY && (*i)->get_position().dist(m_player_pos)<10)
			{
				geo* g=(geo*)*i;
				g->mutate(0.9);
			}
		}
	}

	d_vector gravity;
	list<entity*> newentities;
	vector<list<entity*>::iterator> dellist;
	for (list<entity*>::iterator i=m_world.begin(); i!=m_world.end(); i++)
	{
		(*i)->update(m_world,m_player_pos);
		list<entity*> temp = (*i)->get_new_entities();
		newentities.merge(temp);
		if ((*i)->remove_me()) dellist.push_back(i);

		// contribute to player gravity
		if ((*i)->get_mass())
		{
			d_vector bet=(*i)->get_position()-m_player_pos;
			float distance = bet.mag();
			bet.normalise();
			if (distance<MAX_GRAV_DIST)
			{
				// newton innit? :)
				gravity+=bet*(1-(distance/MAX_GRAV_DIST))*((*i)->get_mass()*0.000005);
			}
		}
	}

	// update player pos
	m_player_vec+=gravity;
	m_player_pos+=m_player_vec;

	if (m_player_pos.x<-200) m_player_pos.x+=400;
	if (m_player_pos.x>200) m_player_pos.x-=400;
	if (m_player_pos.y<-200) m_player_pos.y+=400;
	if (m_player_pos.y>200) m_player_pos.y-=400;

	for (vector<list<entity*>::iterator>::iterator i=dellist.begin(); i!=dellist.end(); i++)
	{
		list<entity*>::iterator e=*i;
		if ((*e)->get_flags()==ENTITYFLAG_ENEMY)
		{
			geo *g=(geo*)*e;
			// keep the genome if it's scored
			if (g->m_genome.score>0)
			{
				m_store.push_back(g->m_genome);
			}
		}

		if ((*e)->get_flags()&ENTITYFLAG_CAUGHT_WEAPONPICKUP)
		{
			m_weapon_level++;
			char buf[10];
			sprintf(buf,"%d",m_weapon_level);
			display_text("WEAPON LEVEL NOW AT "+string(buf),80);
		}

		delete *e;
		m_world.erase(*i);
	}

	m_world.merge(newentities);

	if (m_level_complete)
	{
		m_delay++;
		if (m_delay>LEVELCOMPLETE_DELAY)
		{
			make_new_level();
		}

	}

	if (!m_world.empty()) m_level_complete=true;

	m_recharging=false;

	// player collision
	for (list<entity*>::iterator g=m_world.begin(); g!=m_world.end(); g++)
	{
		entity *other = *g;
		if (m_player_health!=0 && other->get_flags()==ENTITYFLAG_ENEMY)
		{
			if (other->check_collide(m_player_pos,0))
			{
				// landing on enemy increases health...
				m_player_health+=0.1;
				m_recharging=true;
				m_player_vec=-m_player_vec/2.0;
			}
		}

		if (other->get_flags()==ENTITYFLAG_KEY)
		{
			m_level_complete=false;
		}

		else if (other->get_flags()==ENTITYFLAG_EXPLOSION)
		{
			d_vector dist=m_player_pos-other->get_position();
			float d=dist.mag();
			if (d<1)
			{
				m_player_health-=(int)(EXP_DAMAGE_MULT*(1-d));
				m_player_vec+=(dist*(1-d)*EXP_PUSH_MULT);
			}
		}
	}

	if (m_level_complete) display_text("LEVEL COMPLETE!",100);


	if (m_player_health<=0)
	{
		m_player_health=0;
		m_delay++;

		if (m_delay>LIFELOST_DELAY)
		{
			m_player_health=100;
			m_lives--;
			if (m_lives<0)
			{
				display_text("GAME OVER",100);
				m_finished=true;
				bleep::get()->stop_loop(1);
			}
			m_delay=0;
		}
	}

	if (m_player_health>100) m_player_health=100;

	bleep::get()->modify(m_BGsoundID, geo::geo_count, 1);

	m_frame++;
}

void geo_game::render_player()
{
	//gl_polygon_mode(GL_FRONT_AND_BACK,GL_LINE);
	if (m_recharging)
	{
		m_flash_counter++;
		if (m_flash_counter>50) m_flash_counter=0;
		glColor3f(m_flash_counter/50.0f,0,1);
	}
	else glColor3f(1,1,1);
	glPushMatrix();
	glTranslatef(m_player_pos.x,m_player_pos.y,0);
	glScalef(0.5,0.5,0.5);
	glBegin(GL_TRIANGLES);
	glVertex3f(m_player_dir.x,m_player_dir.y,0);
	d_vector cross=m_player_dir.cross(d_vector(0,0,1));
	d_vector v1 = cross-m_player_dir;
	d_vector v2 = (-cross)-m_player_dir;
	glVertex3f(v1.x*0.3,v1.y*0.3,0);
	glVertex3f(v2.x*0.3,v2.y*0.3,0);
	glEnd();
	glPopMatrix();
	//gl_polygon_mode(GL_FRONT_AND_BACK,GL_FILL);
}

void geo_game::renderHUD()
{
	glLoadIdentity();
    glColor4f(1,1,1,1);
    glTranslatef(2.0,-6.5,0);
	char text[256];
	sprintf(text,"population: %d level:%d lives:%d health: %d",geo::geo_count,m_level,m_lives,(int)m_player_health);
	draw_text(text);
}

void geo_game::render()
{
	if (m_first_time)
	{
		glViewport(0,0,m_width,m_height);
		glMatrixMode (GL_PROJECTION);
  		glLoadIdentity();
		//gl_ortho(-1,1,-0.7,0.7,0.1,100);
		glOrtho(-10,10,-7,7,0.1,100);
		glEnable(GL_BLEND);
    	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		glEnable(GL_COLOR_MATERIAL);
		m_first_time=false;
	}

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity();
//    gl_rotatef(75,1,0,0);

//    if(m_player_dir.x) gl_rotatef(-atan2(m_player_dir.y,m_player_dir.x)*RAD_CONV+90,0,0,1);


//    gl_push_matrix();

	// render the scanner



	//gl_scalef(m_scale,m_scale,m_scale);
	glScalef(0.05,0.05,0.05);
	glColor3f(0,0.5,0);
	glTranslatef(-m_player_pos.x,-m_player_pos.y,-10);
	//if(m_player_dir.x) gl_rotatef(-atan(m_player_dir.y/m_player_dir.x)*RAD_CONV,0,0,1);
//    if(m_player_dir.x) gl_rotatef(atan2(m_player_dir.y,m_player_dir.x)*RAD_CONV,0,0,1);
	for(list<entity*>::iterator i=m_world.begin(); i!=m_world.end(); i++)
	{
		// scanner detects enemies
		if ((*i)->get_flags()==ENTITYFLAG_ENEMY)
		{

			glPointSize(2);
			glBegin(GL_POINTS);
			glVertex3f((*i)->get_position().x,(*i)->get_position().y,0);
			glEnd();
		}

		// scanner detects keys
		if ((*i)->get_flags()==ENTITYFLAG_KEY)
		{
			glPushMatrix();
			glTranslatef((*i)->get_position().x,(*i)->get_position().y,0);
			primitive::get()->scanner_key();
			glPopMatrix();
		}
	}
	glPointSize(1);
//    gl_pop_matrix();

//    gl_push_matrix();

	glLoadIdentity();
	glScalef(m_scale,m_scale,m_scale);
	//if(m_player_dir.x) gl_rotatef(-atan(m_player_dir.y/m_player_dir.x)*RAD_CONV,0,0,1);
	glTranslatef(-m_player_pos.x,-m_player_pos.y,-10);

	// render the world
	for(list<entity*>::iterator i=m_world.begin(); i!=m_world.end(); i++)
	{
		if ((*i)->get_flags()==ENTITYFLAG_PARTICLES || m_player_pos.distsq((*i)->get_position())<1000)
		{
			(*i)->render();
		}
	}

	if (m_player_health!=0)
	{
		render_player();
	}
	else // make player flash if life lost
	{
		if (m_flash) render_player();
		if (m_flash_counter>5)
		{
			m_flash_counter=0;
			m_flash=!m_flash;
		}
		m_flash_counter++;
	}

    glPopMatrix();

	renderHUD();
	render_text();

	timeval this_time;
	// stop valgrind complaining
	this_time.tv_sec=0;
	this_time.tv_usec=0;

	gettimeofday(&this_time,NULL);
	m_delta=(this_time.tv_sec-m_last_time.tv_sec)+
			(this_time.tv_usec-m_last_time.tv_usec)*0.000001f;

	if (m_delta<m_deadline)
	{
		//min 1 hz
		if(m_deadline-m_delta<1.0f)
		{
			usleep((int)((m_deadline-m_delta)*1000000.0f));
		}
	}

	m_last_time=this_time;
	//if (m_delta>0) m_time=this_time.tv_sec+this_time.tv_usec*0.000001f;
	if (m_delta>0.0f && m_delta<100.0f) m_time+=m_delta;

}

void geo_game::display_text(string text, int frames)
{
	m_display_text=text;
	m_display_text_frames=frames;
}

void geo_game::render_text()
{
	if (m_display_text_frames>0)
	{
		glLoadIdentity();
    	glColor4f(1,1,1,1);
    	glTranslatef(2.0,0,0);
		draw_text(m_display_text);
	}
	m_display_text_frames--;
}



bool geo_game::finished()
{
	bool temp=m_finished;
	m_finished=false;
	return temp;
}

void geo_game::make_new_level()
{
	clear();
	//m_store.sort();
	bleep::get()->play(m_spawn_sound, 220, 1);
	m_level++;
	m_lives++;
	m_player_vec=d_vector(0,0,0);
	m_delay=0;

	if (!m_store.empty()) // get rid of genomes with score less than the average...
	{
		int average=0;
		for (vector<geo::genome>::iterator i=m_store.begin(); i!=m_store.end(); i++)
		{
			average+=i->score;
		}

		average/=m_store.size();
		vector<geo::genome> newstore;

		for (vector<geo::genome>::iterator i=m_store.begin(); i!=m_store.end(); i++)
		{
			if (i->score>=average) newstore.push_back(*i);
		}

		m_store=newstore;
	}

	m_thrust_particles = new particles(100);
	m_world.push_back(m_thrust_particles);


	float worldsize=m_level*5;
	if (worldsize>50) worldsize=50;

	int numgeos=m_level*20;
	if (numgeos>200) numgeos=200;

	m_world.push_back(new weapon_pickup(d_vector(rand_range(-worldsize*4,worldsize*4),rand_range(-worldsize*4,worldsize*4),0),0.1));

	for (int n=0; n<numgeos; n++)
	{
		d_matrix m;
		m.scale(3,3,3);
		m.translate(rand_range(-worldsize,worldsize),rand_range(-worldsize,worldsize),0);
		geo* newgeo=new geo(0,m);
		if (n<numgeos/2 || m_store.empty())
		{
			newgeo->randomise();
		}
		else
		{
			newgeo->m_genome=m_store[rand()%m_store.size()]; // bias to top?
			newgeo->mutate(0.2);
		}
		m_world.push_back(newgeo);
	}

	int numkeys=m_level+2;
	if (numkeys>20) numkeys=20;

	for (int n=0; n<numkeys; n++)
	{
		d_vector p(rand_range(-worldsize*4,worldsize*4),rand_range(-worldsize*4,worldsize*4),0);
		key* newkey=new key(p,0.05+n/10.0f);
		newkey->set_capture_sound(m_capture_sound);
		m_world.push_back(newkey);
	}
}

void geo_game::init_audio()
{
	feeble::time_length=5;
	feeble::samplerate=22050;

	feeble::time_length=2;
	feeble::value volvalue(0.1);
	feeble::mult volmult;

	// spawn sound
	{
	feeble::envelope env(feeble::envelope::HILL);
	feeble::mult envmult;
	feeble::value envmultvalue(1000);
	feeble::generator master(feeble::generator::SINE);
	feeble::value multvalue(4000);
	feeble::add add;
	feeble::value addvalue(10);
	feeble::mult mult;
	feeble::generator slave(feeble::generator::SINE);

	envmult.patch("a",&envmultvalue);
	envmult.patch("b",&env);
	master.patch("freq",&envmult);
	mult.patch("a",&master);
	mult.patch("b",&multvalue);
	add.patch("a",&mult);
	add.patch("b",&addvalue);
	slave.patch("freq",&add);
	volmult.patch("a",&slave);
	volmult.patch("b",&volvalue);
	volmult.execute();

	m_spawn_sound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
	}

	// shot sound
	{
	feeble::time_length=0.1;
	feeble::envelope env(feeble::envelope::RAMPDOWN);
	feeble::mult envmult;
	feeble::value envmultvalue(5000);
	feeble::generator osc(feeble::generator::SINE);

	envmult.patch("a",&envmultvalue);
	envmult.patch("b",&env);
	osc.patch("freq",&envmult);
	volmult.patch("a",&osc);
	volmult.patch("b",&volvalue);
	volmult.execute();

	m_fire_sound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
	}

	// capture sound
	{
	feeble::time_length=0.1;
	feeble::value modfreq(2);
	feeble::generator mod(feeble::generator::SINE);
	feeble::value modamount(2);
	feeble::value modaddvalue(220);
	feeble::mult mult;
	feeble::add modadd;
	feeble::generator osc1(feeble::generator::SAWTOOTH);
	feeble::value freq2(220);
	feeble::generator osc2(feeble::generator::SAWTOOTH);
	feeble::add add;

	mod.patch("freq",&modfreq);
	mult.patch("a",&mod);
	mult.patch("b",&modamount);
	modadd.patch("a",&mult);
	modadd.patch("b",&modaddvalue);
	osc1.patch("freq",&modadd);
	osc2.patch("freq",&freq2);
	add.patch("a",&osc1);
	add.patch("b",&osc2);
	volmult.patch("a",&add);
	volmult.patch("b",&volvalue);
	volmult.execute();

	m_capture_sound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
	}

	// player explosion sound
	{
	feeble::time_length=0.5;
	feeble::envelope env(feeble::envelope::RAMPUP);
	feeble::mult envmult;
	feeble::value envmultvalue(50);
	feeble::generator osc(feeble::generator::NOISE);

	envmult.patch("a",&envmultvalue);
	envmult.patch("b",&env);
	osc.patch("freq",&envmult);
	volmult.patch("a",&osc);
	volmult.patch("b",&volvalue);
	volmult.execute();

	m_explosion_sound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
	}

	// mutate sound
	{
	feeble::envelope env(feeble::envelope::RAMPDOWN);
	feeble::mult envmult;
	feeble::value envmultvalue(300);
	feeble::generator master(feeble::generator::SINE);
	feeble::value multvalue(40000);
	feeble::add add;
	feeble::value addvalue(440);
	feeble::mult mult;
	feeble::generator slave(feeble::generator::SAWTOOTH);

	envmult.patch("a",&envmultvalue);
	envmult.patch("b",&env);
	master.patch("freq",&envmult);
	mult.patch("a",&master);
	mult.patch("b",&multvalue);
	add.patch("a",&mult);
	add.patch("b",&addvalue);
	slave.patch("freq",&add);
	volmult.patch("a",&slave);
	volmult.patch("b",&volvalue);
	volmult.execute();

	m_mute_sound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
	}

	// bg sound
	{
	feeble::time_length=2;
	feeble::envelope env(feeble::envelope::RAMPDOWN);
	feeble::mult envmult;
	feeble::value envmultvalue(100);
	feeble::generator master(feeble::generator::SINE);
	feeble::value multvalue(100);
	feeble::add add;
	feeble::value addvalue(40);
	feeble::mult mult;
	feeble::generator slave(feeble::generator::SINE);

	envmult.patch("a",&envmultvalue);
	envmult.patch("b",&env);
	master.patch("freq",&envmult);
	mult.patch("a",&master);
	mult.patch("b",&multvalue);
	add.patch("a",&mult);
	add.patch("b",&addvalue);
	slave.patch("freq",&add);
	volmult.patch("a",&slave);
	volmult.patch("b",&volvalue);
	volmult.execute();

	m_BGsound = bleep::get()->register_sound(volmult.get_buffer(),volmult.get_size());
	}
}
