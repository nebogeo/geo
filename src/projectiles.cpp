/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include "glinc.h"
#include "projectiles.h"
#include "geo.h"
#include "bleep.h"
#include "utils.h"

using namespace geogame;

projectile::projectile(const d_vector &pos, const d_vector &vec, const d_vector &inr) :
m_pos(pos),
m_vec(vec),
m_inertia(inr),
m_age(0)
{
  m_flags|=ENTITYFLAG_PROJECTILE;
}

projectile::~projectile()
{
}

void projectile::update(const list<entity*> &world, const d_vector &playerpos)
{
  entity::update(world,playerpos);
  m_pos+=m_vec+m_inertia;

  m_age++;
  if (m_age>400) m_remove_me=true;
}

///////////////////////////////////////////////////////////////////////

bullet::bullet(const d_vector &pos, const d_vector &vec, const d_vector &inr) :
  projectile(pos,vec,inr)
{
}

void bullet::render()
{
  glBegin(GL_LINES);
  glColor3f(1,0,0);
  glVertex3f(m_pos.x,m_pos.y,0);
  d_vector temp=m_vec;
  temp.normalise();
  temp*=0.3;
  glVertex3f(m_pos.x+temp.x,m_pos.y+temp.y,0);
  glEnd();
}

void bullet::update(const list<entity*> &world, const d_vector &playerpos)
{
  projectile::update(world,playerpos);

  for (list<entity*>::const_iterator i=world.begin(); i!=world.end(); i++) {
    if (*i!=this && (*i)->check_collide(m_pos,0,true)) {
      m_remove_me=true;
    }
  }
  if (m_age>100) m_remove_me=true;
}

///////////////////////////////////////////////////////////////////////

missile::missile(const d_vector &pos, const d_vector &vec, const d_vector &inr) :
  projectile(pos,vec,inr),
  m_exploding(false),
  m_fade(1)
{
}

void missile::render()
{
  if (!m_exploding) {
    glBegin(GL_LINES);
    glColor3f(0,1,0);
    glLineWidth(3);
    glVertex3f(m_pos.x,m_pos.y,0);
    d_vector temp=m_vec;
    temp.normalise();
    temp*=0.1;
    glVertex3f(m_pos.x+temp.x,m_pos.y+temp.y,0);
    glEnd();
  } else {
    glColor4f(1,0.5+m_fade/2.0f,m_fade,m_fade);
    glPushMatrix();
    glTranslatef(m_pos.x,m_pos.y,0);
    glScalef(2.0f+(1-m_fade),2.0f+(1-m_fade),0);
    primitive::get()->explosion();
    glPopMatrix();
    m_fade-=0.01;
    if (m_fade<=0) m_remove_me=true;
  }
}

void missile::update(const list<entity*> &world, const d_vector &playerpos)
{
  projectile::update(world,playerpos);

  for (list<entity*>::const_iterator i=world.begin(); i!=world.end(); i++) {
    if (*i!=this) {
      if (!m_exploding && (*i)->check_collide(m_pos,0,true)) {
	bleep::get()->play(m_explode_sound, 220+rand_range(-10,10), 1);
	m_exploding=true;
	m_vec=d_vector(0,0,0);
	m_inertia=d_vector(0,0,0);
	m_flags=ENTITYFLAG_EXPLOSION;
      }
      
      if (m_exploding) (*i)->check_collide(m_pos,3,true);
    }
  }
  if (m_age>100) m_remove_me=true;
}

///////////////////////////////////////////////////////////////////////

rocket::rocket(const d_vector &pos, const d_vector &aimpos, float speed) :
  projectile(pos,d_vector(0,0,0),d_vector(0,0,0)),
  m_exploding(false),
  m_fade(1),
  m_aim_pos(aimpos),
  m_speed(speed)
{
}

void rocket::render()
{
  if (!m_exploding) {
    glPushMatrix();
    glTranslatef(m_pos.x,m_pos.y,0);
    glRotatef(m_age*5,0,0,1);
    glColor4f(1,0,0,0.5);
    float radius=0.55;
    glBegin(GL_TRIANGLES);
    glVertex3f(sin(0.0*DEG_CONV)*0.3,cos(0.0*DEG_CONV)*0.3,0);
    glVertex3f(sin(120.0*DEG_CONV)*0.3,cos(120.0*DEG_CONV)*0.3,0);
    glVertex3f(sin(240.0*DEG_CONV)*0.3,cos(240.0*DEG_CONV)*0.3,0);
    glEnd();
    glPopMatrix();
  } else {
    glColor4f(m_fade,0.5+m_fade/2.0f,1,m_fade);
    glPushMatrix();
    glTranslatef(m_pos.x,m_pos.y,0);
    glScalef(1+(1-m_fade),1+(1-m_fade),0);
    primitive::get()->explosion();
    glPopMatrix();
    if (m_fade<=0) m_remove_me=true;
  }
}

void rocket::update(const list<entity*> &world, const d_vector &playerpos)
{
  projectile::update(world,playerpos);

  if (m_exploding) m_fade-=0.01;

  // collide with player first
  if (!m_exploding && m_pos.distsq(playerpos)<0.25) {
    bleep::get()->play(m_explode_sound, 220+rand_range(-10,10), 1);
    m_exploding=true;
    m_vec=d_vector(0,0,0);
    m_inertia=d_vector(0,0,0);
    m_flags=ENTITYFLAG_EXPLOSION;
  }

  if (m_age>50 && !m_exploding) {
    for (list<entity*>::const_iterator i=world.begin(); i!=world.end(); i++) {
      if (*i!=this) {
	if ((*i)->check_collide(m_pos,0,true) ||
	    ((*i)->get_flags()&ENTITYFLAG_CAUGHTKEY &&
	     m_pos.distsq((*i)->get_position())<1)) {
	  bleep::get()->play(m_explode_sound, 440+rand_range(-10,10), 1);
	  m_exploding=true;
	  m_vec=d_vector(0,0,0);
	  m_inertia=d_vector(0,0,0);
	  m_flags=ENTITYFLAG_EXPLOSION;
	}

	if (m_exploding) (*i)->check_collide(m_pos,3,true);
      }
    }
  }

  if (!m_exploding) {
    d_vector aim=playerpos-m_pos;
    aim.normalise();
    //aim*=0.05;
    aim*=0.005;
    m_vec+=aim;
    //m_vec.normalise();
    //m_vec*=m_speed;
  }

  if (m_age>100 && !m_exploding) m_remove_me=true;
}

bool rocket::check_collide(const d_vector &pos, float radius, bool damage)
{
  return false;
}

///////////////////////////////////////////////////////////////////////

mutator::mutator(const d_vector &pos, const d_vector &vec, const d_vector &inr) :
  projectile(pos,vec,inr),
  m_exploding(false),
  m_size(1)
{
}

void mutator::render()
{
  if (!m_exploding) {
    glBegin(GL_LINES);
    glColor3f(0,1,0);
    glVertex3f(m_pos.x,m_pos.y,0);
    d_vector temp=m_vec;
    temp.normalise();
    temp*=0.1;
    glVertex3f(m_pos.x+temp.x,m_pos.y+temp.y,0);
    glEnd();
  } else {
    float col=1-m_size/5.0f;
    glColor4f(rand_float(),rand_float(),rand_float(),0.5);
    glPushMatrix();
    glTranslatef(m_pos.x,m_pos.y,0);
    glScalef(m_size,m_size,0);
    primitive::get()->explosion();
    glPopMatrix();
    m_size+=1;
    if (m_size>=10) m_remove_me=true;
  }
}

void mutator::update(const list<entity*> &world, const d_vector &playerpos)
{
  projectile::update(world,playerpos);

  for (list<entity*>::const_iterator i=world.begin(); i!=world.end(); i++)
    {
      if (*i!=this)
	{
	  if (!m_exploding && (*i)->check_collide(m_pos,0,true))
	    {
	      bleep::get()->play(m_explode_sound, 220+rand_range(-10,10), 1);
	      m_exploding=true;
	      m_vec=d_vector(0,0,0);
	      m_inertia=d_vector(0,0,0);
	      m_flags=ENTITYFLAG_EXPLOSION;
	    }

	  if (m_exploding &&
	      (*i)->get_flags()&ENTITYFLAG_ENEMY &&
	      (*i)->check_collide(m_pos,m_size,false))
	    {
	      geo* g=(geo*)*i;
	      g->mutate(0.1);
	    }
	}
    }
}
