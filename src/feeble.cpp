/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include <math.h>
#include <stdlib.h>
#include "feeble.h"
#include <iostream>

#ifdef WIN32
#define M_PI 3.14152
#endif

using namespace geogame;

unsigned int feeble::samplerate = 44100;
float feeble::time_length = 1;

feeble::feeble()
{
}

feeble::~feeble()
{
}

/////////////////////////////////////////////////////////////////////////////

feeble::module::module() :
m_buffer(NULL),
m_size(0),
m_samplerate(feeble::samplerate),
m_time_length(feeble::time_length)
{
}

feeble::module::~module()
{
	if(m_buffer) delete[] m_buffer;
}

float feeble::module::get(unsigned int n)
{
	if (n<m_size) return m_buffer[n];
	else return 0;
}

float feeble::module::execute()
{
	for (map<string,module*>::iterator i=m_inputs.begin(); i!=m_inputs.end(); i++)
	{
		m_time_length = i->second->execute();
	}

	return m_time_length;
}

float feeble::module::get_input(const string &name, unsigned int n)
{
	map<string,module*>::iterator i=m_inputs.find(name);
	if (i!=m_inputs.end())
	{
		return i->second->get(n);
	}
	return 0;
}

void feeble::module::dump()
{
	for (unsigned int n=0; n<m_size; n++)
	{
		cerr<<m_buffer[n]<<endl;
	}
}

/////////////////////////////////////////////////////////////////////////////

float feeble::value::execute()
{
	m_size=(unsigned int)(m_time_length*m_samplerate);
	return m_time_length;
}

/////////////////////////////////////////////////////////////////////////////

float feeble::mult::execute()
{
	feeble::module::execute();

	unsigned int samplelength = (unsigned int)(m_time_length*m_samplerate);

	if (m_size!=samplelength || !m_buffer)
	{
		if (m_buffer) delete[] m_buffer;
		m_buffer = new float[samplelength];
		m_size=samplelength;
	}

	for (unsigned int n=0; n<m_size; n++)
	{
		m_buffer[n] = get_input("a",n)*get_input("b",n);
	}

	return m_time_length;
}

/////////////////////////////////////////////////////////////////////////////

float feeble::add::execute()
{
	feeble::module::execute();

	unsigned int samplelength = (unsigned int)(m_time_length*m_samplerate);

	if (m_size!=samplelength || !m_buffer)
	{
		if (m_buffer) delete[] m_buffer;
		m_buffer = new float[samplelength];
		m_size=samplelength;
	}

	for (unsigned int n=0; n<m_size; n++)
	{
		m_buffer[n] = get_input("a",n)+get_input("b",n);
	}

	return m_time_length;
}

/////////////////////////////////////////////////////////////////////////////

feeble::generator::generator(type t) :
m_type(t)
{
}

float feeble::generator::execute()
{
	feeble::module::execute();

	unsigned int samplelength = (unsigned int)(m_time_length*m_samplerate);

	if (m_size!=samplelength || !m_buffer)
	{
		if (m_buffer) delete[] m_buffer;
		m_buffer = new float[samplelength];
		m_size=samplelength;
	}

	float position=0;
	float cyclepos=0;
	float dampfactor=20; // samples
	float randsandh=0;

	for (unsigned int n=0; n<m_size; n++)
	{
		float freq = get_input("freq",n);
		float phase = get_input("phase",n);

		position+=phase+M_PI*freq/m_samplerate;
		cyclepos=fmod((float)position,(float)M_PI)/M_PI;

		switch (m_type)
		{
			case feeble::generator::SINE:     m_buffer[n]=sin(position); break;
			case feeble::generator::SAWTOOTH: m_buffer[n]=cyclepos; break;
			case feeble::generator::REVSAW:   m_buffer[n]=1-cyclepos; break;
			case feeble::generator::TRIANGLE:
				if (cyclepos<0.5) m_buffer[n]=cyclepos*2;
				else m_buffer[n]=(1-cyclepos)*2;
			break;
			case feeble::generator::NOISE:
				// hardly scientific
			 	if ((int)freq==0 || n%(int)freq==0) randsandh=(rand()%1000/500.0f)-1.0f;
				m_buffer[n]=randsandh;
			break;
			case feeble::generator::SQUARE: break;
		}

		// dampen begin and end of sample to stop clicking
		if (n<dampfactor) m_buffer[n]*=n/dampfactor;
		else if (n>m_size-dampfactor) m_buffer[n]*=(m_size-n)/dampfactor;
	}

	return m_time_length;
}

////////////////////////////////////////////////////////////////////

feeble::envelope::envelope(type t) :
m_type(t)
{
}

float feeble::envelope::execute()
{
	feeble::module::execute();

	unsigned int samplelength = (unsigned int)(m_time_length*m_samplerate);

	if (m_size!=samplelength || !m_buffer)
	{
		if (m_buffer) delete[] m_buffer;
		m_buffer = new float[samplelength];
		m_size=samplelength;
	}

	for (unsigned int n=0; n<m_size; n++)
	{
		float time = n/(float)m_samplerate;
		float t=n/(float)m_size;

		switch (m_type)
		{
			case feeble::envelope::RAMPUP:   m_buffer[n]=t; break;
			case feeble::envelope::RAMPDOWN: m_buffer[n]=1-t; break;
			case feeble::envelope::HILL:
				if (t<0.5) m_buffer[n]=t*2;
				else m_buffer[n]=(1-t)*2;
			break;
			case feeble::envelope::VALLEY:
				if (t<0.5) m_buffer[n]=(1-t)*2;
				else m_buffer[n]=t*2;
			break;
		}

	}

	return m_time_length;
}
