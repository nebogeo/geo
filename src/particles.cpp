/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include <GL/glut.h>
#include "particles.h"
#include "utils.h"

using namespace geogame;

particles::particle::particle(const d_vector &p,const d_vector &d,const d_vector &i):
position(p),
direction(d),
inertia(i),
age(0)
{
}


particles::particles(int maxparticles) :
m_max_particles(maxparticles)
{
	m_flags|=ENTITYFLAG_PARTICLES;
}

particles::~particles()
{
}

void particles::update(const list<entity*> &world, const d_vector &playerpos)
{
	for (list<particle>::iterator i=m_particles.begin(); i!=m_particles.end(); i++)
	{
		i->position+=i->direction+i->inertia;
		i->inertia*=0.999;
		i->age++;
	}
}

void particles::render()
{
	for (list<particle>::iterator i=m_particles.begin(); i!=m_particles.end(); i++)
	{
		if (i->age<100)
		{
			float size=0.5+(i->age/50.0f);
			size*=size;
			glColor4f(1/(i->age*0.01f),1/(i->age*0.05f),i->age/100.0f,(1-(i->age/100.0f))/5.0f);
			glPushMatrix();
			glTranslatef(i->position.x,i->position.y,0);
			glScalef(size,size,size);
			primitive::get()->particle();
			glPopMatrix();
		}
	}
}

void particles::add_particle(const particle &p)
{
	if (m_particles.size()>(unsigned int)m_max_particles)
	{
		m_particles.erase(m_particles.begin());
	}

	m_particles.push_back(p);
}
