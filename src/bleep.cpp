/*  Copyright (C) 2015 Dave Griffiths <dave@fo.am>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
 */

#include <iostream>
#include <limits.h>
#include "bleep.h"

using namespace geogame;
using namespace std;

bleep *bleep::m_singleton=NULL;

bleep::bleep() :
m_current_source(0)
{
  m_device=alcOpenDevice(NULL);
  if(m_device==NULL) {
    cerr<<"could not open openAL device"<<endl;
    return;
  }

  int attrlist[]={ALC_FREQUENCY,44100,ALC_INVALID};
  m_context=alcCreateContext(m_device,attrlist);
  if(m_context==NULL) {
    cerr<<"could not open openAL context: "<<alGetString(alcGetError(m_device))<<endl;
    return;
  }

  alcMakeContextCurrent(m_context);

  ALfloat zeroes[] = { 0.0f, 0.0f,  0.0f };
  ALfloat front[]  = { 0.0f, 0.0f,  1.0f, 0.0f, 1.0f, 0.0f };
  alListenerfv(AL_POSITION, zeroes);
  alListenerfv(AL_ORIENTATION, front);

  alGenSources(MAX_SOURCES,m_sources);
  alGenSources(MAX_SOURCES,m_looping_sources);
}

bleep::~bleep()
{
  alcDestroyContext(m_context);
  alcCloseDevice(m_device);
}

bleep *bleep::get()
{
  if (!m_singleton) m_singleton = new bleep;
  return m_singleton;
}

int bleep::register_sound(float *data, unsigned int size)
{
  short *buf = new short[size];
  for (unsigned int n=0; n<size; n++) {
    buf[n]=(short)(data[n]*SHRT_MAX);
  }

  return register_sound(buf,size);
}

int bleep::register_sound(short *data, unsigned int size)
{
  ALuint buffer;
  alGenBuffers(1, &buffer);
  alBufferData(buffer, AL_FORMAT_MONO16, data, size*sizeof(short), 44100);
  if(alGetError() != AL_NO_ERROR) {
    cerr<<"could not buffer_data"<<endl;
    return 1;
  }

  return buffer;
}

int bleep::play(int id, float freq, float vol, int loop)
{
  int sid;
  if (loop==0) {
    sid=m_sources[m_current_source];
    m_current_source++;
    if (m_current_source>=MAX_SOURCES) m_current_source=0;
    alSourceStop(sid);
  } else {
    sid=m_looping_sources[loop];
    alSourceStop(sid);
  }

  ALfloat zeroes[] = { 0.0f, 0.0f,  0.0f };
  alSourcei(sid, AL_BUFFER, id);
  alSourcefv(sid,AL_POSITION,zeroes);
  if (loop) alSourcei(sid, AL_LOOPING, AL_TRUE);
  alSourcef(sid, AL_PITCH, freq/440.f);
  alSourcePlay(sid);
  return sid;
}

void bleep::modify(int pid, float freq, float vol)
{
  alSourcef(pid, AL_PITCH, freq/440.f);
}

void bleep::stop_loop(int loopid)
{
  alSourceStop(m_looping_sources[loopid]);
}
