/*  dada
 *  copyright (C) 2000 david griffiths <dave@blueammonite.f9.co.uk>
 *
 *  this program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU general public license as published by
 *  the free software foundation; either version 2 of the license, or
 *  (at your option) any later version.
 *
 *  this program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
 *  GNU general public license for more details.
 *
 *  you should have received a copy of the GNU general public license
 *  along with this program; if not, write to the free software
 *  foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.
*/ 
// dada.h 
// A collection of handy classes for 3D graphics.
// (mostly half finished)
//


#include <math.h>
#include <iostream>
#include <list>
#ifndef DADA
#define DADA

using namespace std;
namespace geogame {

static const float TWO_PI=3.141592654*2.0f;
static const float DEG_CONV = 0.017453292;
static const float RAD_CONV = 1/0.017453292;

inline void debug(char *s) {cerr<<"dada debug: "<<s<<endl;}

void  init_dada();
float rand_float();
float rand_range(float L, float H);
void  d_sin_cos(float a, float &s, float &c);

class d_vector
{
public:
       float x,y,z,w;
       d_vector() {x=y=z=0; w=1;}
       d_vector(float X, float Y, float Z, float W=1) {x=X; y=Y; z=Z; w=W;}
       d_vector(d_vector const &c) {*this=c;}
		
	float *arr() {return &x;}
	int operator==(d_vector const &rhs) {return (x==rhs.x&&y==rhs.y&&z==rhs.z);}
       d_vector &operator=(d_vector const &rhs);
       d_vector operator+(d_vector const &rhs) const;
       d_vector operator-(d_vector const &rhs) const;
       d_vector operator*(float rhs) const;
       d_vector operator/(float rhs) const;
       d_vector &operator+=(d_vector const &rhs);
       d_vector &operator-=(d_vector const &rhs);
       d_vector &operator*=(float rhs);
       d_vector &operator/=(float rhs);
       d_vector cross(d_vector const &rhs) const;
       float dot(d_vector const &rhs) const;
       float dist(d_vector const &rhs) const;
       float distsq(d_vector const &rhs) const;
       float mag();
       float magsq();
       void get_euler(float &rx, float &ry, float &rz) const;
       void homog() {if (w && w!=1.0) {x/=w; y/=w; z/=w; w=1;}}
       d_vector &normalise() {*this/=mag(); return *this;}
		 
		 void get_rot(float m[16],d_vector up); // legacy func 
private:
};

d_vector operator-(d_vector rhs);
ostream &operator<<(ostream &os, d_vector const &om);
istream &operator>>(istream &is, d_vector &om);

////

class d_colour
{
public:
       float r,g,b,a;
       d_colour() {r=g=b=0; a=1;}
       d_colour(float R, float G, float B, float A=1) {r=R; g=G; b=B; a=A;}
       d_colour(d_colour const &c) {*this=c;}
	float *arr() {return &r;}

       d_colour &operator=(d_colour const &rhs);
       d_colour operator+(d_colour const &rhs);
       d_colour operator-(d_colour const &rhs);
       d_colour operator*(float rhs);
       d_colour operator/(float rhs);
       d_colour &operator+=(d_colour const &rhs);
       d_colour &operator-=(d_colour const &rhs);
       d_colour &operator*=(float rhs);
       d_colour &operator/=(float rhs);

       void clamp()
       {
           if (r<0) r=0; if (g<0) g=0; if (b<0) b=0; if (a<0) a=0;
           if (r>1) r=1; if (g>1) g=1; if (b>1) b=1; if (a>1) a=1;
       }

private:
};

ostream &operator<<(ostream &os, d_colour const &om);

////

class d_vertex
{
public:
    d_vertex() {}
    d_vertex(d_vector p, d_vector n, float S=0, float T=0) {point=p; normal=n; s=S; t=T;}
    d_vertex(d_vector p, d_vector n, d_colour c, float S=0, float T=0) { point=p; normal=n; col=c; s=S; t=T;}
    d_vertex(d_vertex const &rhs) {*this=rhs;}
    d_vertex const &operator=(d_vertex const &rhs);
    void homog() {point.homog(); normal.homog();}
    friend ostream&operator<<(ostream &os, d_vertex const &v);

    d_vector point;
    d_vector normal;
    d_colour col;
    float s,t;
private:
};

class d_matrix
{
public:
    d_matrix() {init();}
	d_matrix(float m00, float m10, float m20, float m30, 
			float m01, float m11, float m21, float m31, 
			float m02, float m12, float m22, float m32, 
			float m03, float m13, float m23, float m33)
			{
			m[0][0]=m00; m[1][0]=m10; m[2][0]=m20; m[3][0]=m30;
			m[0][1]=m01; m[1][1]=m11; m[2][1]=m21; m[3][1]=m31;
			m[0][2]=m02; m[1][2]=m12; m[2][2]=m22; m[3][2]=m32;
			m[0][3]=m03; m[1][3]=m13; m[2][3]=m23; m[3][3]=m33;
			}

    void init();
    float *arr() {return &m[0][0];}
    const d_matrix &operator=(d_matrix const &rhs);
    d_matrix operator+(d_matrix const &rhs);
    d_matrix operator-(d_matrix const &rhs);
    d_matrix operator*(d_matrix const &rhs);
    d_matrix operator/(d_matrix const &rhs);
    d_matrix &operator+=(d_matrix const &rhs);
    d_matrix &operator-=(d_matrix const &rhs);
    d_matrix &operator*=(d_matrix const &rhs);
    d_matrix &operator/=(d_matrix const &rhs);
	d_matrix &translate(d_vector &tr);
    d_matrix &translate(float x, float y, float z);
    void    settranslate(d_vector &tr);
    d_vector gettranslate();
    d_matrix &rotx(float a);
    d_matrix &roty(float a);
    d_matrix &rotz(float a);
    d_matrix &rotxyz(float x,float y,float z);
    d_matrix &scale(float x, float y, float z);
    d_vector transform_no_trans(d_vector const &p) const;
    d_vector transform(d_vector const &p) const;
    d_vertex transform(d_vertex const &p) const;
	void    transpose();
	d_matrix inverse();
	float   determinant();
	d_vector get_hori_i() {return d_vector(m[0][0],m[1][0],m[2][0]);}
	d_vector get_hori_j() {return d_vector(m[0][1],m[1][1],m[2][1]);}
	d_vector get_hori_k() {return d_vector(m[0][2],m[1][2],m[2][2]);}
	d_vector get_vert_i() {return d_vector(m[0][0],m[0][1],m[0][2]);}
	d_vector get_vert_j() {return d_vector(m[1][0],m[1][1],m[1][2]);}
	d_vector get_vert_k() {return d_vector(m[2][0],m[2][1],m[2][2]);}
	void    remove_scale();
	void    extract_euler(float &x, float &y, float &z);
	void    aim(d_vector v, d_vector up=d_vector(0,0,1));
	
	void load_glmatrix(float glm[16]);
	void load_d_matrix(float glm[16]);
	
    friend ostream &operator<<(ostream &os, d_matrix const &om);
//private:
    float m[4][4];
};

ostream &operator<<(ostream &os, d_matrix const &om);

class d_bounding_box
{
public:
	d_bounding_box() : m_empty(true) {}
	virtual ~d_bounding_box() {}
	
	void clear() { min=d_vector(0,0,0); max=d_vector(0,0,0); m_empty=true; }
	bool empty() { return m_empty; }
	void expand(d_vector v);
	void expand(d_bounding_box v);
	void expandby(float a);
	bool inside(d_vector point) const;
	bool inside(d_vector p, float radius) const;
	bool inside(d_bounding_box v) const;

	d_vector min;
	d_vector max;
	
private:
	bool m_empty;
};

////

}

#endif // DADA
